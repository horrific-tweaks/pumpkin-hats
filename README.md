This datapack adds a set of custom hat models for the carved pumpkin item. These all have crafting recipies which allow them to be obtained in survival; most regular hats are craftable by combining a carved pumpkin with a certian dye color.

<details>
  <summary>Regular Hats</summary>

  - black dye → top hat
  - blue dye → wizard hat
  - green dye → froggy hat
  - purple dye → witch hat
  - white dye → party hat
  - yellow dye → construction helmet
</details>

<details>
  <summary>Special Hats</summary>

  - glass block → space helmet
  - glass pane → diving helmet
</details>

<details>
  <summary>Secret Hats</summary>

  - oak sapling → oak tree disguise
  - spruce sapling → spruce tree disguise
</details>

## Credits

The first model under the 'Secret Hats' dropdown is adapted from [Eli95's resource pack](https://www.planetminecraft.com/texture-pack/tree-costume-from-hermitcraft/).
